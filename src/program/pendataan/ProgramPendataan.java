/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package program.pendataan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Audi
 */
public class ProgramPendataan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        frmlogin f = new frmlogin();
        f.setVisible(true);
    }
    public static String iduser;
    public static String namauser;
    public static String tipeuser;
    public static String ipkeserver = "localhost";
    public static int portkeserver = 8888;

    public boolean Login(String username, String password) {
        boolean status = false;
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "login";
            output.println(jenisdata);

            output.println(username);
            output.println(password);

            String statuslogin = input.readLine();
            if (statuslogin.equalsIgnoreCase("true")) {
                status = true;
                iduser = input.readLine();
                namauser = input.readLine();
                tipeuser = input.readLine();
            } else {
                status = false;
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public boolean InsertKegiatan(String noinduk, String kegiatan, String tanggal) {
        boolean status = false;
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "insertkegiatan";
            output.println(jenisdata);

            output.println(iduser);
            output.println(noinduk);
            output.println(kegiatan);
            output.println(tanggal);

            String statusinsert = input.readLine();
            if (statusinsert.equalsIgnoreCase("true")) {
                status = true;
            } else {
                status = false;
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public String gantiPassword(String iduser, String passwordlama, String passwordbaru) {
        String status = "";
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "gantipassword";
            output.println(jenisdata);

            output.println(iduser);
            output.println(passwordlama);
            String sama = input.readLine();
            if (sama.equalsIgnoreCase("true")) {
                output.println(passwordbaru);
                String statusgantipassword = input.readLine();
                if (statusgantipassword.equalsIgnoreCase("true")) {
                    status = "berhasil";
                } else {
                    status = "gagal";
                }
            } else {
                status = "passwordtidaksama";
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public ArrayList AmbilDataLogMenuAdmin() {
        ArrayList list = new ArrayList<>();
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "menulog";
            output.println(jenisdata);

            int sizeArray = Integer.parseInt(input.readLine());
            for (int i = 0; i < sizeArray; i++) {
                Log l = new Log();
                l.setNama(input.readLine());
                l.setEvent(input.readLine());
                l.setWaktu(input.readLine());
                list.add(l);
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList AmbilDataLogDenganTanggal(String tanggal, String bulan, String tahun) {
        ArrayList list = new ArrayList<>();
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "tanggallog";
            output.println(jenisdata);

            output.println(tanggal);
            output.println(bulan);
            output.println(tahun);

            int sizeArray = Integer.parseInt(input.readLine());
            for (int i = 0; i < sizeArray; i++) {
                Log l = new Log();
                l.setNama(input.readLine());
                l.setEvent(input.readLine());
                l.setWaktu(input.readLine());
                list.add(l);
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public String AmbilDataHakAkses(String noinduk) {
        String data = "";
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "datahakspesial";
            output.println(jenisdata);

            output.println(noinduk);
            if (input.readLine().equalsIgnoreCase("true")) {
                data = input.readLine();
                data += ",";
                data += input.readLine();
            } else {
                data = "";
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    public boolean UpdateHakAkses(String noinduk, String pilihan) {
        boolean status = false;
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "updatehakspesial";
            output.println(jenisdata);

            output.println(iduser);
            output.println(noinduk);
            output.println(pilihan);
            if (input.readLine().equalsIgnoreCase("true")) {
                status = true;
            } else {
                status = false;
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public String RequestDataDiriDikitnya(String noinduk) {
        String datadiri = "";
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "datadiridikit";
            output.println(jenisdata);

            output.println(noinduk);
            if (input.readLine().equalsIgnoreCase("true")) {
                String nik = input.readLine();
                String nama = input.readLine();
                String alias = input.readLine();
                String alamat = input.readLine();
                String jeniskelamin = input.readLine();
                String tanggallahir = input.readLine();
                datadiri = nik + "," + nama + "," + alias + "," + alamat + "," + jeniskelamin + "," + tanggallahir;
            } else {
                datadiri = "";
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datadiri;
    }

    public ArrayList AmbilDataKegiatan(String noinduk) {
        ArrayList list = new ArrayList<>();
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "datakegiatan";
            output.println(jenisdata);

            output.println(noinduk);

            int sizeArray = Integer.parseInt(input.readLine());
            for (int i = 0; i < sizeArray; i++) {
                Kegiatan k = new Kegiatan();
                k.setNamakegiatan(input.readLine());
                k.setTanggal(input.readLine());
                list.add(k);
            }

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void LogOut(String id) {
        Socket s;
        String address = ipkeserver;
        int port = portkeserver;
        PrintStream output;
        BufferedReader input;
        try {
            s = new Socket(address, port);
            output = new PrintStream(s.getOutputStream());
            input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String jenisdata = "logout";
            output.println(jenisdata);

            output.println(id);

        } catch (IOException ex) {
            Logger.getLogger(ProgramPendataan.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
